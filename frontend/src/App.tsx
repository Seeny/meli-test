import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import routes from "./shared/routes/routes";

import "./App.scss";

function App() {
  const GetRoutes = () => {
    if (routes && routes.length) {
      return routes.map((route, index) => <Route key={index} {...route} />);
    }
  };

  return (
    <Router forceRefresh={false}>
      <Switch>{GetRoutes()}</Switch>
    </Router>
  );
}

export default App;
