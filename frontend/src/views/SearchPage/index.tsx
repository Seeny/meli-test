import React, { useEffect } from "react";
import Header from "../../shared/components/Header/Header";
import UserItem from "../../shared/components/UserItem/UserItem";
import BreadCrumbs from "../../shared/components/BreadCrumbs/BreadCrumbs";
import { props } from "../../shared/types/types";
import useMyStore from "../../shared/zustand";
import "../../App.scss";

const useStore = useMyStore;

const SearchPage: React.FC<props> = (props) => {
  let urlParam = new URLSearchParams(props.location.search).get("search");

  const response = useStore((state) => state.items);
  const getItems = useStore((state) => state.getItems);
  const isLoading = useStore((state) => state.isLoading);

  useEffect(() => {
    // el item se puede tomar directamente de los params o si se a guardado previamente en la Store
    (async function () {
      //una validacion simple para evitar rupturas de codigo y asegurar que se ejecute la accion
      if (urlParam) {
        await getItems(urlParam);
      }
    })();
  }, [urlParam]);

  useEffect(() => {
    (async function () {
      //una validacion simple para evitar rupturas de codigo y asegurar que se ejecute la accion
      if (urlParam) {
        await getItems(urlParam);
      }
    })();
  }, [urlParam]);

  const renderItem = (): undefined | JSX.Element[] | JSX.Element => {
    if (response && response.items?.length) {
      //mapeo de los items traidos por el apikey para ser enviados el componente correspondiente
      return response.items.map((item, index) => (
        <UserItem key={item.id} {...item} />
      ));
    } else if (isLoading) {
      return <div className="loading">Cargando...</div>;
    }
  };

  return (
    <div>
      <Header />
      <main className="content-wrapper">
        <BreadCrumbs categories={response.categories} />
        <div className="search-results">{renderItem()}</div>
      </main>
    </div>
  );
};

export default SearchPage;
