import { useEffect } from "react";
import { useParams } from "react-router-dom";
import UserDetail from "../../shared/components/UserDetail/UserDetail";

import useMyStore from "../../shared/zustand";
import Header from "../../shared/components/Header/Header";
import BreadCrumbs from "../../shared/components/BreadCrumbs/BreadCrumbs";

const useStore = useMyStore;

interface IUserPublicProfileRouteParams {
  id: string;
}

const ItemDetail = () => {
  const { id } = useParams<IUserPublicProfileRouteParams>();

  const getcatecories = useStore((state) => state.categories);
  const getItemDetail = useStore((state) => state.getItemDetail);
  const itemdetail = useStore((state) => state.itemdetail);

  useEffect(() => {
    (async function () {
      await getItemDetail(id);
    })();
  }, []);

  const renderDetail = () => {
    if (itemdetail) {
      return (
        <div>
          <Header />
          <main className="content-wrapper">
            <BreadCrumbs categories={getcatecories} />
            <div className="item-wrapper">
              <UserDetail {...itemdetail} />
            </div>
          </main>
        </div>
      );
    }
  };

  return <div>{renderDetail()}</div>;
};

export default ItemDetail;
