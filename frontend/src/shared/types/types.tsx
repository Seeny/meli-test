import { State } from "zustand";

export interface MyUserStore extends State {
  searchItem: string | undefined;
  items: ItemResponse;
  isLoading: boolean;
  itemdetail: ItemDetail | undefined;
  categories: string[] | undefined;
  getItems: (item: string) => void;
  getItemDetail: (itemId: string | number) => void;
  setSearchItem: (item: string) => void;
}

export interface props {
  item: string;
  location: {
    hash: string;
    key: string;
    pathname: string;
    search: string;
  };
}
export interface ItemResponse {
  author?: Author;
  categories?: string[];
  items?: Item[];
}

export interface Author {
  name?: string;
  lastname?: string;
}

export interface Item {
  id?: string;
  title?: string;
  condition?: string;
  price?: Price;
  picture?: string;
  free_shipping?: boolean;
  address?: string;
  sold_quantity?: number;
  category_id?: string;
}

export interface Price {
  currency?: string;
  amount?: number;
  decimals?: number;
}

export interface ItemDetail {
  id?: string;
  title?: string;
  condition?: string;
  price?: Price;
  picture?: string;
  free_shipping?: boolean;
  address?: null;
  sold_quantity?: number;
  category_id?: string;
  description?: string;
}

export interface Price {
  currency?: string;
  amount?: number;
  decimals?: number;
}
