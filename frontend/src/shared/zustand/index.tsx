import create, { SetState, GetState } from "zustand";
import { MyUserStore } from "../types/types";

export default create(
  (
    setState: SetState<MyUserStore>,
    getState: GetState<MyUserStore>
  ): MyUserStore => {
    return {
      items: {},
      itemdetail: undefined,
      isLoading: false,
      searchItem: undefined,
      categories: [],
      setSearchItem: async (item: string) => {
        setState({ searchItem: item });
      },
      getItems: async (item: string) => {
        setState({ isLoading: true, items: {} });
        const result = await fetch(
          `http://localhost:3001/items?search=${item}`
        );
        const items = await result.json();
        setState({ items, categories: items.categories });
        console.log(items);
        setState({ isLoading: false });
      },
      getItemDetail: async (itemId: string | number) => {
        setState({ isLoading: true });
        const result = await fetch(`http://localhost:3001/items/${itemId}`);
        const itemdetail = await result.json();
        setState({ itemdetail, isLoading: false });
      },
    };
  }
);
