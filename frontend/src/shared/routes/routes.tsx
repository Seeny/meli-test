import Home from "../../views/HomePage/Home";
import ItemDetail from "../../views/ItemDetail";
import NotFound from "../../views/NotFound/notFound";
import SearchPage from "../../views/SearchPage";

const routes = [
  {
    path: "/",
    component: Home,
    exact: true,
  },
  {
    path: "/items/:id",
    component: ItemDetail,
    exact: false,
  },
  {
    path: "/items",
    component: SearchPage,
    exact: false,
  },
  {
    path: "/404",
    component: NotFound,
  },
];

export default routes;
