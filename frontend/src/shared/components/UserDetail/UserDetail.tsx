import React from "react";
import { ItemDetail } from "../../types/types";
import useMyStore from "../../zustand";
import Header from "../Header/Header";

import "../../assets/styles/sass/itemDetail.scss";

const useStore = useMyStore;

export default React.memo(function UserDetail({
  id,
  title,
  condition,
  price,
  picture,
  free_shipping,
  address,
  sold_quantity,
  category_id,
  description,
}: ItemDetail) {
  const isLoading = useStore((state) => state.isLoading);

  if (isLoading) {
    return <div className="loading">Cargando...</div>;
  } else {
    return (
      <div>
        <div className="item-picture">
          <img src={picture} alt={title} />
        </div>
        <div className="item-details">
          <span className="item-condition">
            {condition} - {sold_quantity}{" "}
          </span>
          <h2 className="item-name">{title}</h2>
          <h3 className="item-price">{price?.amount}</h3>
          <button className="buy-button">Comprar</button>
        </div>
        {description && (
          <div className="item-description">
            <h4>Descripción del Producto</h4>
            <p>{description}</p>
          </div>
        )}
      </div>
    );
  }
});
