import React from "react";
import "../../assets/styles/sass/item.scss";

import { ItemResponse } from "../../types/types";

export default React.memo(function BreadCrumbs({ categories }: ItemResponse) {
  if (categories) {
    return (
      <ul className="breadcrumbs">
        {categories.map((category) => (
          <li key={category}>{category}</li>
        ))}
      </ul>
    );
  } else {
    return null;
  }
});
