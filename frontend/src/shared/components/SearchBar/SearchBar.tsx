import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import useMyStore from "../../zustand";

const useStore = useMyStore;

const SearchBar = () => {
  const [searchValue, setSearchValue] = useState("");
  const setSearchItem = useStore((state) => state.setSearchItem);

  const history = useHistory();

  const searchItems = (event: React.FormEvent) => {
    event.preventDefault();
    setSearchItem(searchValue);
    history.push(`/items?search=${searchValue}`);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(event.target.value);
  };

  return (
    <form className="search-form" onSubmit={searchItems}>
      <input
        type="text"
        value={searchValue}
        className="search-input"
        placeholder="Nunca Dejes de buscar"
        onChange={handleChange}
      />
      <button type="submit" className="search-button"></button>
    </form>
  );
};

export default SearchBar;
