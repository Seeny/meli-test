import React from "react";
import "../../assets/styles/sass/item.scss";

import { useHistory } from "react-router-dom";
import { Item } from "../../types/types";
import shipping from "../../assets/images/ic_shipping.png";

export default React.memo(function UserItem({
  id,
  title,
  condition,
  price,
  picture,
  free_shipping,
  address,
  sold_quantity,
  category_id,
}: Item) {
  const history = useHistory();

  const handleClick = () => {
    history.push(`/items/${id}`);
  };

  return (
    <div className="item" onClick={handleClick}>
      <div className="item__general">
        <img src={picture} alt={title} />
        <div className="item__resume">
          <div className="item__resume--title">
            <h1>{price?.amount}</h1>
            {free_shipping ? <img src={shipping} alt="Free shipping" /> : null}
          </div>
          <h2>{title}</h2>
        </div>
      </div>
      <span>{address}</span>
    </div>
  );
});
