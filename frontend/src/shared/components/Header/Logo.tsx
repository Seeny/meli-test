import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/images/Logo_ML.png";
import "../../assets/styles/sass/Header.scss";

const Logo = () => {
  return (
    <Link to="/">
      <img src={logo} alt="MercadoLibre" title="MercadoLibre" />
    </Link>
  );
};

export default Logo;
