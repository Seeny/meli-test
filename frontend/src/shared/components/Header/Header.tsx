import React from "react";
import SearchBar from "../SearchBar/SearchBar";
import Logo from "./Logo";
import "../../assets/styles/sass/Header.scss";

const Header = () => {
  return (
    <header>
      <nav>
        <Logo />
        <SearchBar />
      </nav>
    </header>
  );
};

export default Header;
