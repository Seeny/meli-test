# Test Front-End Mercadolibre by Stephen Barreto

La aplicación consta de tres componentes principales: 

-la cajade búsqueda
-la visualización de resultados
-la descripción del detalle del producto

Esta App se realizo con el siguiente stack como extra se uso el typado stricto de TypeScript para una escalabilidad, ya que al usar un tipado fuerte nos ayuda a tener buenas practicas de desarrollo  y aumentar el rendimiento en la producccion de nuevos componentes para nuestra app.

Adicional: 

* [Zustand](https://zustand.surge.sh/) - Como alternativa a Redux para el manejo de la store de nuestra App.

### Stack 📋

```
//Front-end
React
Sass
Axios
Html
TypeScript

//Back-en
Node.js
Express
```

### Instalación 🔧

### Servidor

```bash
cd backend
npm install
npm start
```

Por defecto, el backend recibe peticiones por `http://localhost:3001/`

### Cliente

```bash
cd frontend
npm install
npm start
```

Por defecto, el servidor de desarrollo abre en `http://localhost:3000/`

### Compiles and minifies for production
```
npm run build
```

## TO-DO 📌


- Manejo de links en el BreadCrumbs para que el usuario se pueda dirigir a la categoria anterior.
- Cambiar imagen por una adaptada a la pantalla ya que es muy pequeña.
- Manejo de typado para cada respuesta en archivos separados (si el proyecto crece).
- Implementar Manejo de Errores en el servidor.


## Autore ✒️

* **Stephen Barreto** 

