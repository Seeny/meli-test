var express = require("express");
const axios = require("axios");
const { response } = require("express");
const { mapItem, mapResponse } = require("./setters");

var app = express();
var port = 3001;
const MELI_API_ITEMS = "https://api.mercadolibre.com/items/";
const MELI_API_SEARCH = "https://api.mercadolibre.com/sites/MLA/search?q=";

app.use((req, res, next) => {
  const allowedDomains = ["http://localhost:3000", "http://0.0.0.0:3000"];
  const { origin } = req.headers;
  if (allowedDomains.indexOf(origin) > -1) {
    res.setHeader("Access-Control-Allow-Origin", origin);
  }
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, OPTIONS, DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/items/:id", async function (req, res) {
  try {
    const response_item = await axios.get(MELI_API_ITEMS + req.params.id);
    const response_description = await axios.get(
      MELI_API_ITEMS + req.params.id + "/description"
    );

    const response = mapItem(response_item.data);
    response.description = response_description.data.plain_text; // Ver formato del text
    res.json(response);
  } catch (error) {
    console.log(error);
    if (error.response) {
      res
        .status(error.response.status)
        .json({ error: error.response.data.message });
    }
  }
});

app.get("/items", async function (req, res) {
  try {
    const response = await axios.get(
      `https://api.mercadolibre.com/sites/MLA/search?q=${req.query.search}`
    );
    const categories = response.data.filters.find(
      (filter) => filter.id === "category"
    );

    res.json(mapResponse(response.data.results, categories));
  } catch (error) {
    console.log(error);
    if (error.response) {
      res
        .status(error.response.status)
        .json({ error: error.response.data.message });
    }
  }
});

app.listen(port);
console.log("Running app on port port. Visit: http://localhost:" + port + "/");
